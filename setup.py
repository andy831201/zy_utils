from setuptools import setup

setup(
    name='zy_utils',
    version='0.0.5',
    install_requires=[
        'torch',
        'plotly'
    ]
)