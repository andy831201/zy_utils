# %%
import torch
from torch.utils.data import DataLoader

def train(model: torch.nn.Module, 
            epochs: int, 
            train_loader: DataLoader, 
            device: torch.device, 
            optimizer: torch.optim, 
            loss_fn: torch.nn,
            print_freq: int):
    """
    deeplearning model training function

    Args:
        model: torch模型, 
        epochs: 訓練次數, 
        train_loader: 訓練集dataloader, 
        device: 訓練時使用之硬體(cpu or gpu), 
        optimizer: 優化器, 
        loss_fn: loss function

    Returns:
        model: 訓練完畢之模型
        loss_list: 訓練時loss,供後續視覺化時使用

    """
    loss_list = []
    model = model.to(device)
    model.train()
    for epoch in range(1, epochs+1):
        for batch_idx, (data, target) in enumerate(train_loader):
            data, target = data.to(device), target.to(device)

            optimizer.zero_grad()
            output = model(data)

            loss = loss_fn(output, target)
            loss.backward()
            optimizer.step()

            if batch_idx % print_freq == 0:
                loss_list.append(loss.item())
                batch = batch_idx * len(data)
                data_count = len(train_loader.dataset)
                precentage = (100. * batch_idx / len(train_loader))
                print(f"Epoch {epoch}: [{batch:5d} / {data_count}] ({precentage:.0f} %), Loss: {loss.item():.6f}")

    return model, loss_list                    


# %%
if __name__ == '__main__':
    train
# %%
