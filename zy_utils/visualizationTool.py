# %%
import plotly.express as px
import pandas as pd

import matplotlib.pyplot as plt
import numpy as np


def bar_chart(data:pd.DataFrame, x_col:str, y_col:str, fig_show=True):
    fig = px.bar(data, x=x_col, y=y_col)
    if fig_show:
        fig.show()
    return fig

def loss_chart(data:list, fig_show=True):
    fig = px.line(data)
    if fig_show:
        fig.show()
    return fig


def evolution_chart(imgs, ori_img, with_ori=True, row_title=None, **imshow_kwargs):
    if not isinstance(imgs[0], list):
        imgs = [imgs]
    
    num_rows = len(imgs)
    num_cols = len(imgs[0]) + with_ori

    fix, axs = plt.subplots(nrows=num_rows, ncols=num_cols, squeeze=False)
    for row_idx, row in enumerate(imgs):
        row = [ori_img] + row if with_ori else row
        for col_idx, img in enumerate(row):
            ax = axs[row_idx, col_idx]
            ax.imshow(np.asarray(img), **imshow_kwargs)
            ax.set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])

    if with_ori:
        axs[0, 0].set(title="origin image")
        axs[0, 0].title.set_size(8)
    
    if row_title is not None:
        for row_idx in range(num_rows):
            axs[row_idx, 0].set(ylabel=row_title[row_idx])

    plt.tight_layout()


# %%
if __name__ == '__main__':
    from zy_utils.visualizationTool import bar_chart, loss_chart
    bar_chart(pd.DataFrame([['1', '3'], ['2', '4']], columns=['a', 'b']), 'a', 'b')
    loss_chart([1,2,3])
# %%
